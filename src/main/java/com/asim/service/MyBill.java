package com.asim.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Formatter;
import java.util.Iterator;
import java.util.List;

import com.asim.dto.Categories;
import com.asim.dto.Category;
import com.asim.dto.FlatDiscountSlab;
import com.asim.dto.FlatDiscountSlabs;
import com.asim.dto.Item;
import com.asim.dto.Items;

public class MyBill {

	public void getBill(Items cart, FlatDiscountSlabs discountSlabs, Categories categories) {
		List<Item> items = cart.getItems();
		BigDecimal sum = new BigDecimal(0).setScale(2, RoundingMode.CEILING);
		int count = 0;
		try (Formatter formatter = new Formatter()) {
			System.out.println(formatter.format("%1$4s   %2$-25s   %3$10s   %4$-8s   %5$-11s   %6$-13s", "#", "Name",
					"Price", "Quantity", "Total Price", "Discount Price"));
		}

		BigDecimal totalDiscount = new BigDecimal(0).setScale(2, RoundingMode.CEILING);
		for (Item item : items) {
			try (Formatter formatter = new Formatter()) {
				String categoryId = item.getCategoryId();
				BigDecimal categoryDiscountPercent = getCategoryDiscount(categoryId,categories.getCategoryList());
				BigDecimal discount = item.getTotalCost().multiply(categoryDiscountPercent).divide(new BigDecimal(100))
						.setScale(2, RoundingMode.CEILING);
				totalDiscount = totalDiscount.add(discount).setScale(2, RoundingMode.CEILING);
				;
				BigDecimal netTotalCost = item.getTotalCost().subtract(discount).setScale(2, RoundingMode.CEILING);
				System.out.println(formatter.format("%1$3s.   %2$-25s   %3$10s   %4$8s   %5$11s   %6$14s", ++count,
						item.getName(), item.getPrice(), item.getQuantity(), item.getTotalCost(), netTotalCost));
				sum = sum.add(netTotalCost);
			}
		}

		BigDecimal slabDiscount = new BigDecimal(0).setScale(2, RoundingMode.CEILING);
		BigDecimal eligibleSum = sum;
		Iterator<FlatDiscountSlab> iterator = discountSlabs.getSlabs().iterator();
		while (iterator.hasNext()) {
			FlatDiscountSlab slab = iterator.next();
			if ((0 <= eligibleSum.compareTo(slab.getRangeMin())) && (null == slab.getRangeMax())
					|| (0 <= eligibleSum.compareTo(slab.getRangeMin())
							&& 0 >= eligibleSum.compareTo(slab.getRangeMax()))) {
				eligibleSum = eligibleSum.subtract(slab.getRangeMin());
				BigDecimal discount = eligibleSum.multiply(slab.getDiscountPercent()).divide(new BigDecimal(100));
				slabDiscount = slabDiscount.add(discount);
			}
		}

		totalDiscount = totalDiscount.add(slabDiscount).setScale(2, RoundingMode.CEILING);
		;

		BigDecimal netTotal = sum.subtract(slabDiscount).setScale(2, RoundingMode.CEILING);
		StringBuilder sb = new StringBuilder();
		try (Formatter formatter = new Formatter(sb)) {
			System.out.println(formatter.format("%1$70s  %2$15s", "Total", sum));
			sb.setLength(0);
			System.out.println(
					formatter.format("%1$70s  %2$15s", "Total Applied discount(Category + Slab)", totalDiscount));
			sb.setLength(0);
			System.out.println(formatter.format("%1$70s  %2$15s", "Discount Price", netTotal));
		}
	}

	private BigDecimal getCategoryDiscount(String id,List<Category> catLst) {
		for (Category category : catLst) {
			if (category.getId().equals(id)) {
				return category.getDiscountPercent();
			}
		}
		return new BigDecimal(0);
	}
	
	
}
