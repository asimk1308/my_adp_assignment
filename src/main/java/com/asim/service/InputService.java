package com.asim.service;

import com.asim.dto.Categories;
import com.asim.dto.FlatDiscountSlabs;
import com.asim.dto.Items;

public interface InputService {
	public Categories getCategoryDiscounts();

	public FlatDiscountSlabs getDiscountSlabs();

	public Items getShoppingCart();
}
