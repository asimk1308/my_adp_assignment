package com.asim.service;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.asim.constants.Iconstants;
import com.asim.dto.Categories;
import com.asim.dto.FlatDiscountSlabs;
import com.asim.dto.Items;

/**
 * @author Asim Khan
 *
 */
public class InputServiceImpl implements InputService {


	@Override
	public Categories getCategoryDiscounts() {
		InputStream is = Iconstants.CLASS_LOADER.getResourceAsStream(Iconstants.CATEGORIES_FILE);
		JAXBContext jaxbContext;
		Categories categories = new Categories();
		try {
			jaxbContext = JAXBContext.newInstance(Categories.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			categories = (Categories) jaxbUnmarshaller.unmarshal(is);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return categories;
	}

	@Override
	public FlatDiscountSlabs getDiscountSlabs() {
		InputStream is = Iconstants.CLASS_LOADER.getResourceAsStream(Iconstants.FLAT_DISCOUNT_SLABS_FILE);
		JAXBContext jaxbContext;
		FlatDiscountSlabs flatDisocuntSlabs = new FlatDiscountSlabs();
		try {
			jaxbContext = JAXBContext.newInstance(FlatDiscountSlabs.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			flatDisocuntSlabs = (FlatDiscountSlabs) jaxbUnmarshaller.unmarshal(is);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return flatDisocuntSlabs;
	}

	@Override
	public Items getShoppingCart() {
		InputStream is = Iconstants.CLASS_LOADER.getResourceAsStream(Iconstants.SHOPPING_CART_FILE);
		JAXBContext jaxbContext;
		Items cart = new Items();
		try {
			jaxbContext = JAXBContext.newInstance(Items.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			cart = (Items) jaxbUnmarshaller.unmarshal(is);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return cart;
	}
}
