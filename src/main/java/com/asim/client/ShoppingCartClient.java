package com.asim.client;

import com.asim.dto.Categories;
import com.asim.dto.FlatDiscountSlabs;
import com.asim.dto.Items;
import com.asim.service.InputService;
import com.asim.service.InputServiceImpl;
import com.asim.service.MyBill;

/**
 * @author Asim Khan
 *
 */
public class ShoppingCartClient {
	public static void main(String[] args) {
		InputService inputService = new InputServiceImpl();

		Categories categories = inputService.getCategoryDiscounts();
		FlatDiscountSlabs slabs = inputService.getDiscountSlabs();
		Items cart = inputService.getShoppingCart();

		MyBill my = new MyBill();
		my.getBill(cart, slabs, categories);
	}
}
