package com.asim.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Categories")
public class Categories {
	List<Category> categoryList = new ArrayList<>();

	public List<Category> getCategoryList() {
		return categoryList;
	}

	@XmlElement(name = "Category")
	public void setCategoryList(List<Category> categoryList) {
		this.categoryList = categoryList;
	}

}
