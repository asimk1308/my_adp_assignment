package com.asim.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "FlatDiscountSlabs")
public class FlatDiscountSlabs {
	private List<FlatDiscountSlab> slabs = new ArrayList<>();

	public List<FlatDiscountSlab> getSlabs() {
		return slabs;
	}

	@XmlElement(name = "Slab")
	public void setSlabs(List<FlatDiscountSlab> slabs) {
		this.slabs = slabs;
	}
}
