package com.asim.constants;

import com.asim.client.ShoppingCartClient;

public class Iconstants {
	public static final String CATEGORIES_FILE = "categories.xml";
	public static final String FLAT_DISCOUNT_SLABS_FILE = "flatDiscountSlabs.xml";
	public static final String SHOPPING_CART_FILE = "shoppingCart.xml";
	public static final ClassLoader CLASS_LOADER = ShoppingCartClient.class.getClassLoader();
}
