package com.asim.test;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.asim.dto.Categories;
import com.asim.dto.FlatDiscountSlabs;
import com.asim.dto.Items;
import com.asim.service.InputService;
import com.asim.service.InputServiceImpl;

public class InputServiceImplTest {
	private static InputService inputService;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		inputService = new InputServiceImpl();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		inputService = null;
	}

	@Test
	public void testGetCategoryDiscounts() {
		Categories categories = inputService.getCategoryDiscounts();
		assertNotNull(categories);
		assertNotNull(categories.getCategoryList());
		assertTrue(categories.getCategoryList().size() == 5);
	}

	@Test
	public void testGetDiscountSlabs() {
		FlatDiscountSlabs discountSlabs = inputService.getDiscountSlabs();
		assertNotNull(discountSlabs);
		assertNotNull(discountSlabs.getSlabs());
		assertTrue(discountSlabs.getSlabs().size() == 3);
	}

	@Test
	public void testGetShoppingCart() {
		Items cart = inputService.getShoppingCart();
		assertNotNull(cart);
		assertNotNull(cart.getItems());
		assertTrue(cart.getItems().size() == 4);
	}

}
